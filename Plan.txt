=============== Database ====================================
User
    userId
    username
    email
    password
    createDate
    updateDate
    status  = Active, Inactive
    
Profile
    userId
    firstName
    lastName
    avatar
    cover
    phone
    sex
    birthday
    type    = Model, Photographer, Studio
    createDate
    updateDate
    
Photo
    userId
    categoryId
    source
    description
    createDate
    updateDate
    type    = Flickr, Facebook, Local, Other
    status  = Active, Inactive

Favorite
    userId
    destinationId
    createDate
    updateDate
    type    = Photo, Blog
    status  = Active, Inactive

Category
    userId
    title
    description
    type    = Blog, Album
    createDate
    updateDate
    status  = Active, Inactive
Blog    
    userId
    categoryId
    title
    description
    content
    type,
    createDate
    updateDate
    status  = Active, Inactive
    
Status
    userId
    content
    createDate
    updateDate
    status  = Active, Inactive
    
Friend
    userId
    friendId
    createDate
    updateDate
    type    = Close, Normal
    status  = Waiting, Friend, Block
    
Comment
    userId
    destinationId   = Photo, Post, Album, Comment
    content
    createDate
    updateDate
    type    = Photo, Post, Album, Comment
    status  = Active, Edited
    
Emotion
    userId
    destinationId
    createDate
    updateDate
    type    = Wow, Nice, Love
    status  = Waiting, Friend, Block

    

    

    
Fixed =================
1, Cannot find module '../build/Release/bson']
=> 
	change path to js version in catch block
	bson = require('../build/Release/bson');
	to
	bson = require('../browser_build/bson');
    
    
2, E:\Development\Workspace\WebStorm\HapiStack\server\node_modules\hapi\node_module
s\hoek\lib\index.js:731
throw new Error(msgs.join(‘ ‘) || ‘Unknown error’);
Error: Unknown handler: directory
=>
    You need the INERT module to make the routes work.
    Instead of “server.route(Routes.endpoints);” line add the following code in your server.js file and it will work
    server.register(Inert, function(err){
    if(err){
    throw err;
    }
    server.route(Routes.endpoints);
    });



Reference =====================
https://github.com/agendor/sample-hapi-rest-api
