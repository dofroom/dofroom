var Hapi = require('hapi'),
    Routes = require('./routes'),
    Db = require('./config/db'),
    Inert = require('inert'),
    Bcrypt = require('bcrypt'),
    Basic = require('hapi-auth-basic');
    Auth = require('./controller/auth'),
    Vision = require('vision'),
    Config = require('./config/config');
    
    
var app = {};
app.config = Config;

//For older version of hapi.js
//var server = Hapi.createServer(app.config.server.host, app.config.server.port, {cors: true});

var server = new Hapi.Server();

server.connection({ port: app.config.server.port });

server.register([Inert, Vision], function (err) {
    if (err) console.log(err);
});

server.route(Routes.endpoints);


server.register(Basic, (err) => {
    server.auth.strategy('simple', 'basic', { validateFunc: Auth });
    server.route({
        method: 'GET',
        path: '/',
        config: {
            auth: 'simple',
            handler: function (request, reply) {
                reply('hello, ' + request.auth.credentials.name);
            }
        }
    });

    server.start(() => {
        console.log('server running at: ' + server.info.uri);
    });
});


server.start(function () {
  console.log('Server started ', server.info.uri);
});