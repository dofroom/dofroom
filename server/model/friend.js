
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Friend
  * @description a Friend relationship
*/

var FriendSchema = new Schema({
  
  userId: { type: String, required: true },
  friendId: { type: String, required: true },
  type: { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var Friend = mongoose.model('Friend', FriendSchema);

/** export schema */
module.exports = {
    Friend : Friend
};