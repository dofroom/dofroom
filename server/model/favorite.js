var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Favorite
  * @description The favorite photo or blog of user 
*/

var FavoriteSchema = new Schema({
  
  userId: { type: String, required: true },
  categoryId: { type: String, required: true },
  source: { type: String, required: true },
  description: { type: String },
  type    : { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var Favorite = mongoose.model('favorite', FavoriteSchema);

/** export schema */
module.exports = {
    Favorite : Favorite
};