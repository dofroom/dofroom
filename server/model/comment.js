var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Comment
  * @description The Comment photo, blog, status, comment 
*/

var CommentSchema = new Schema({
  
  userId: { type: String, required: true },
  destinationId: { type: String, required: true },
  content: { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  type    : { type: String, required: true },
  status:  { type: String }
});

var Comment = mongoose.model('Comment', CommentSchema);

/** export schema */
module.exports = {
    Comment : Comment
};