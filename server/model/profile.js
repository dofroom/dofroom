var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Profile
  * @description contain profile about user
*/

var ProfileSchema = new Schema({
  
  userId: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  avatar: { type: String },
  cover: { type: String },
  phone: { type: String },
  sex: { type: Boolean },
  birthday: { type: Date },
  type    : { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  
});

var profile = mongoose.model('profile', ProfileSchema);

/** export schema */
module.exports = {
    Profile : profile
};