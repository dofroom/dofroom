
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Blog
  * @description a Blog of blog or album
*/

var BlogSchema = new Schema({
  
  userId: { type: String, required: true },
  categoryId: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String },
  content: { type: String, required: true },
  type    : { type: String},
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var Blog = mongoose.model('blog', BlogSchema);

/** export schema */
module.exports = {
    Blog : Blog
};