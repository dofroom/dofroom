var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Photo
  * @description a photo of user
*/

var PhotoSchema = new Schema({
  
  userId: { type: String, required: true },
  destinationId: { type: String, required: true },
  type    : { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var photo = mongoose.model('photo', PhotoSchema);

/** export schema */
module.exports = {
    Photo : photo
};