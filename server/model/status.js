
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Status
  * @description a Status of Status or album
*/

var StatusSchema = new Schema({
  
  userId: { type: String, required: true },
  content: { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var Status = mongoose.model('Status', StatusSchema);

/** export schema */
module.exports = {
    Status : Status
};