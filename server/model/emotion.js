var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Emotion
  * @description The Emotion photo, blog, status, Emotion 
*/

var EmotionSchema = new Schema({
  
  userId: { type: String, required: true },
  destinationId: { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  type    : { type: String, required: true },
  status:  { type: String }
});

var Emotion = mongoose.model('Emotion', EmotionSchema);

/** export schema */
module.exports = {
    Emotion : Emotion
};