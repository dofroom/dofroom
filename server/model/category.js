var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  Category
  * @description a category of blog or album
*/

var CategorySchema = new Schema({
  
  userId: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  type    : { type: String, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  status:  { type: String }
});

var Category = mongoose.model('category', CategorySchema);

/** export schema */
module.exports = {
    Category : Category
};