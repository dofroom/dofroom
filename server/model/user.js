var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
  * @module  User
  * @description contain information to authenticate
*/

var UserSchema = new Schema({
  userId : { type: String, unique: true, required: true },
  username : { type: String, required: true },
  email : { type: String, required: true },
  password : { type: String, required: true },
  createDate : { type: Date, required: true },
  updateDate : { type: Date, required: true },
  status: { type: String, required: true }
});

var user = mongoose.model('user', UserSchema);

/** export schema */
module.exports = {
    User : user
};